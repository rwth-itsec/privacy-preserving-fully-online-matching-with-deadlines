# Privacy-Preserving Fully Online Matching with Deadlines

This repository contains the main source code of the protocols presented in the paper "Privacy-Preserving Fully Online Matching with Deadlines" (Andreas Klinger, Ulrike Meyer, 2023, CODASPY 2023).



## Simple Test Execution

Local Execution only, no network simulation.

The following example is for 10 parties, change accordingly for different number of parties

1. Download MP-SPDZ version 0.3.3 (a newer version should also work)
   https://github.com/data61/MP-SPDZ/releases/download/v0.3.3/mp-spdz-0.3.3.tar.xz
2. Extract it and execute `Scripts/tldr.sh` and `Scripts/setup-ssl.sh 10`
3. Copy the `*.mpc` files to `Programs/Source`
4. Change in the `*.mpc` files the line `N = 10` to number of parties you want to test. Analogously change `L = 10` for the input vector size
5. Compile the programs `python3 compile.py arrival_sed` , `python3 compile.py deadline` , ... . Note `sed` is used to denote the matching condition based on "Squared Euclidean Distance", and `eq` is for the one based on the "number of equal entries".
6. Create dummy inputs (the "9 = N-1" is for `N=10` parties, the `210 = (L+1* n )*n` with `N=10` number of parties):
   `for i in $(seq 0 9); do seq -s ' ' 210 > Player-Data/Input-P${i}-0 ; done`
7. In `Scripts/shamir.sh` change the line `export PLAYERS=${PLAYERS:-3}` to `export PLAYERS=${PLAYERS:-10}` (10 stands for `N=10` parties)
   and the line `run_player shamir-party.x $* $t || exit 1` to `run_player shamir-party.x $* $t  --bits-from-squares --batch-size 1000 || exit 1`
8. Execute FIRST the arrival, then the deadline, so that the state is stored.
   `Scripts/shamir.sh arrival_sed` and then `Scripts/shamir.sh deadline`
   or
   `Scripts/shamir.sh arrival_appendix` and then `Scripts/shamir.sh deadline_appendix_sed`




## Acknowledgements

This work is partly funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) – 2236/2.
